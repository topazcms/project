<?php

return [
	'multi' => [
		'admin' => [
			'driver' =>	'eloquent',
			'model' =>	'Topaz\Core\Models\User',
		],
	],

	'model' => 'Topaz\Core\Models\User',
	'table' => 'topaz_users',

	'password' => [
		'email' => 'emails.password',
		'table' => 'password_resets',
		'expire' => 60,
	],

];
