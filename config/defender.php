<?php

/**
 * Defender - Laravel 5 ACL Package
 * Author: PHP Artesãos.
 */
return [

    /*
     * Default Role model used by Defender.
     */
    'role_model' => Artesaos\Defender\Role::class,

    /*
     * Default Permission model used by Defender.
     */
    'permission_model' => Artesaos\Defender\Permission::class,

    /*
     * Roles table name
     */
    'role_table' => 'topaz_roles',

    /*
     *
     */
    'role_key' => 'role_id',

    /*
     * Permissions table name
     */
    'permission_table' => 'topaz_permissions',

    /*
     *
     */
    'permission_key' => 'permission_id',

    /*
     * Pivot table for roles and users
     */
    'role_user_table' => 'topaz_role_user',

    /*
     * Pivot table for permissions and roles
     */
    'permission_role_table' => 'topaz_permission_role',

    /*
     * Pivot table for permissions and users
     */
    'permission_user_table' => 'topaz_permission_user',

    /*
     * Forbidden callback
     */
    'forbidden_callback' => Artesaos\Defender\Handlers\ForbiddenHandler::class,

    /*
     * Use blade template helpers
     */
    'template_helpers' => true,

    /*
     * Use helper functions
     */
    'helpers' => true,

    /*
     * Super User role name
     */
    'superuser_role' => 'superadmin',

    /*
     * js var name
     */
    'js_var_name' => 'defender',

];
