<?php namespace Topaz\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		\Topaz\Core\Middleware\TopazInitSites::class,
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'Topaz\Http\Middleware\VerifyCsrfToken',
        'Topaz\Core\Middleware\TopazMinify'
//		'GrahamCampbell\HTMLMin\Http\Middleware\MinifyMiddleware',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'Topaz\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'Topaz\Http\Middleware\RedirectIfAuthenticated',
	];

}
